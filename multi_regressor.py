import numpy as np
from sklearn.svm import SVR
import matplotlib.pyplot as plt
from sklearn.metrics import r2_score
from sklearn.pipeline import make_pipeline
from sklearn.ensemble import RandomForestRegressor
from sklearn.preprocessing import PolynomialFeatures
from sklearn.model_selection import train_test_split
from sklearn.linear_model import BayesianRidge, Ridge, LinearRegression


def regressor(X, y, algorithm, bayes=False):
    
    # splitting
    X_train, X_test = X[:215], X[215:]
    y_train, y_test = y[:215], y[215:]
    
#     # special case of bayes_ridge algorithm
#     if bayes:
#         X_train = np.vander(
#                 np.array(X_train).flatten(),
#                 degree + 1,
#                 increasing=True
#                 )
#         X_test = np.vander(
#                 np.array(X_test).flatten(),
#                 degree + 1,
#                 increasing=True
#                 )

    # fit the data
    algorithm.fit(X=X_train, y=y_train)

    # make the prediction using regression
    train_predict = algorithm.predict(X_train)
    test_predict = algorithm.predict(X_test)

    # calculate the r2 score for the training and testing
    train_score = r2_score(y_train, train_predict)
    test_score = r2_score(y_test, test_predict)

    # plot the data and regression along with scores
    plt.plot(range(215), list(train_predict), label="Training Prediction")
    plt.plot(range(215), list(y_train), 'o', label="Training Data")
    plt.plot(range(215, len(y_test)+215), list(test_predict), label="Test Prediction")
    plt.plot(range(215, len(y_test)+215), list(y_test), 'o', label="Test Data")

    plt.plot([], [], ' ', label="Training Score: "+str(round(train_score, 2)))
    plt.plot([], [], ' ', label="Testing Score: "+str(round(test_score, 2)))

    plt.xlabel("Year")
    plt.ylabel("Total Births")

    plt.legend(bbox_to_anchor=(1, 1))

    # save the plot produced
    filename = str(input("Path to save plot: "))
    plt.savefig(filename, bbox_inches='tight')


def linear(X, y):
    """Linear regression algorithm."""
    algorithm = LinearRegression()
    regressor(X, y, algorithm)


def ridge(X, y):
    """Ridge regression algorithm."""
    algorithm = Ridge()
    regressor(X, y, algorithm)


def forest(X, y, n_estimators=100):
    """Random Forest regression algorithm.
    Available args: n_estimators"""
    algorithm = RandomForestRegressor(n_estimators)
    regressor(X, y, algorithm)


def polynomial(X, y, degree=3):
    """Polynomial regression algorithm.
    Available args: degree"""
    algorithm = make_pipeline(PolynomialFeatures(degree), LinearRegression())
    regressor(X, y, algorithm)


# def bayes_ridge(X, y, t_size=0.2, r_state=0, degree=3, fit_intercept=False):
#     """Bayesian Ridge regression algorithm.
#     Available args: X, y, t_size, r_state, degree, fit_intercept"""
#     algorithm = BayesianRidge(fit_intercept=fit_intercept)
#     regressor(X, y, t_size, r_state, algorithm, degree, bayes=True)


def svr(X, y):
    """Support vector regression algorithm."""
    algorithm = SVR()
    regressor(X, y, algorithm)
